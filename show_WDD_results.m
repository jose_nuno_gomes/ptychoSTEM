function [] = show_WDD_results(ptycho)
% to plot the results from WDD reconstruction
% this function needs comments + add labels

    %display WDD amplitude and phase
    figure;
    subplot(1,2,1);
    imagesc(imrotate(abs(ptycho.psi),180));
    axis image; colorbar; colormap gray;
    title(['WDD ABS, \epsilon = ',num2str(ptycho.eps_ratio)])
    subplot(1,2,2);
    imagesc(imrotate(angle(ptycho.psi),180));
    axis image; colorbar; colormap gray
    title(['WDD Phase, \epsilon = ',num2str(ptycho.eps_ratio)])

    %compare SSB phase image to WDD phase image
    if ptycho.varfunctions.single_side_band_reconstruction
    figure;
    subplot(1,2,1);
    imagesc(angle(ptycho.trotterimgR));
    axis image; colorbar;
    h=colorbar;
    title('SSB');
    set(h,'fontsize',22);
    axis off;
    subplot(1,2,2);
    imagesc(imrotate(angle(ptycho.psi),0));
    axis image; colorbar
    h=colorbar;
    title('WDD');
    set(h,'fontsize',22);
    axis off;
    colormap gray
    end
    
    figure;
    subplot(1,2,1);
    imagesc(ptycho.Q_x,ptycho.Q_y,log10(1+abs(ptycho.Psi)));
    axis image; colorbar; 
    title(['Wigner DD, \epsilon = ',num2str(ptycho.eps_ratio)])
    % xlabel('Q_p [rad]'); ylabel('Q_p [rad]')
    subplot(1,2,2);
    imagesc(ptycho.Q_x,ptycho.Q_y,angle(ptycho.Psi));
    axis image; colorbar
    % xlabel('Q_p [rad]'); ylabel('Q_p [rad]')
    % colormap gray
    
end
