function ptycho = single_trotter_inspector(ptycho,QpHighMod)

% this function needs comments

ptycho = define_real_space(ptycho);

for i =1:size(QpHighMod,1)

xx = QpHighMod(i,2);
yy = QpHighMod(i,1);

g =  squeeze(ptycho.G_wp(:,:,yy,xx));
Q_x_rot = ptycho.Q_x(xx).*cos(ptycho.rot_angle) - ptycho.Q_y(yy).*sin(ptycho.rot_angle);
Q_y_rot = ptycho.Q_x(xx).*sin(ptycho.rot_angle) + ptycho.Q_y(yy).*cos(ptycho.rot_angle); % + skew_k * Q_x_rot ;

[dx,dy] = meshgrid(ptycho.kx_wp + Q_x_rot , ptycho.ky_wp + Q_y_rot);
d1 = sqrt(dx.*dx+dy.*dy);
[dx,dy] = meshgrid(ptycho.kx_wp - Q_x_rot , ptycho.ky_wp - Q_y_rot);
d2 = sqrt(dx.*dx+dy.*dy);
[dx,dy] = meshgrid(ptycho.kx_wp, ptycho.ky_wp);
d3 = sqrt(dx.*dx+dy.*dy);

g_ampL = abs(g);
g_ampL(d1>ptycho.ObjApt_angle)=0;
g_ampL(d2<ptycho.ObjApt_angle)=0;
g_ampL(d3>ptycho.ObjApt_angle)=0;
g_ampR = abs(g);
g_ampR(d1<ptycho.ObjApt_angle)=0;
g_ampR(d2>ptycho.ObjApt_angle)=0;
g_ampR(d3>ptycho.ObjApt_angle)=0;
g_amp = g_ampR + g_ampL;

if yy==fix(ptycho.ObjSize(1)/2)+1 && xx==fix(ptycho.ObjSize(2)/2)+1
    g_amp = abs(g);
    g_ampL = abs(g);
    g_ampR = abs(g);
end

g_phaseL = angle(g);
g_phaseL(d1>ptycho.ObjApt_angle)=0;
g_phaseL(d2<ptycho.ObjApt_angle)=0;
g_phaseL(d3>ptycho.ObjApt_angle)=0;
g_phaseR = angle(g);
g_phaseR(d1<ptycho.ObjApt_angle)=0;
g_phaseR(d2>ptycho.ObjApt_angle)=0;
g_phaseR(d3>ptycho.ObjApt_angle)=0;
g_phase = g_phaseL + g_phaseR;

figure;
subplot(2,2,1)
imagesc(abs(g)); axis square ;
set(gca,'YTick',[]); set(gca,'XTick',[]);
colorbar;
title(['[',num2str(yy),'/',num2str(xx),']','Q=',num2str(ptycho.Q(yy,xx)*1000),'mrad']);
subplot(2,2,2)
imagesc(angle(g)); axis square ;
set(gca,'YTick',[]); set(gca,'XTick',[]);
colorbar;
subplot(2,2,3)
imagesc(g_amp); axis square;
set(gca,'YTick',[]); set(gca,'XTick',[]);
colorbar;
subplot(2,2,4)
imagesc(g_phase); axis square ;
set(gca,'YTick',[]); set(gca,'XTick',[]);
colorbar;
end

end