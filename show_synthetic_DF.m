function [ptycho] = show_synthetic_DF(ptycho)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% To calculate and show synthethic Dark Field image. 
% Function requires structured array ptycho. Figures are displayed if
% ptycho.plot_figures is set to 1. 
% Input variables required are:
% ptycho.m: 4D array that contains the ronchigram for each probe position
% ptycho.pacbed: matrix containing the Position Averaged Convergent Beam
% Electron Diffraction (PACBED) pattern.
% ptycho.maskDF: matrix of mask for Incoherent Bright Field detector
% If ptycho.maskDF is not available, it will calculate it. This will require the
% following variables:
%   ptycho.ObjApt_angle: Probe convergence angle
%   ptycho.theta: matrix of scattering angles
% Output variables added to ptycho are:
% ptycho.DFimg: matrix of Annular Bright Field image
% ptycho.varfunctions.show_synthetic_DF: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ptycho.varfunctions.show_synthetic_DF %check if function has been run
    
    figure;
    imagesc(ptycho.DFimg); axis image; axis off; title('DF')
    colormap gray
    
else
    if ptycho.varfunctions.calculate_detector_masks
        maskDF = ptycho.maskDF;
    else
        ptycho = calculate_detector_masks(ptycho);
        maskDF = ptycho.maskDF;
    end
    
    % calculate ADF image
    
    DFimg  = zeros(size(ptycho.m,3),size(ptycho.m,4));
    
    for yy=1:size(ptycho.m,3)
        for xx = 1:size(ptycho.m,4)
            %ADF
            img = ptycho.m(:,:,yy,xx).*maskDF;
            DFimg(yy,xx) = sum(img(:));
        end
    end
    
    if ptycho.plot_figures
        figure
        imagesc(ptycho.pacbed.*maskDF);axis image;
        title('ADF detector')
        
        figure;
        imagesc(DFimg); axis image; axis off;  title('DF')
        colormap gray
        
    end
    
    ptycho.DFimg = DFimg;
    ptycho.varfunctions.show_synthetic_DF = 1;
    
end

end

