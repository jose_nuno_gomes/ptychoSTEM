function [RonchiRoadmap] = RonchiRoadmapFunc_v1()
% This function generates a roadmap for each detector ronchigram frame in the raw datasets. 
% it builds the link between the probe position in the ADF scan and its correponding detector 
% ronchigram and their location of the raw-data files.

% Output: 
% structural variable RonchiRoadmap 
% RonchiRoadmap.Flag        - logic variable that assignes each frame into the images.
%                             The value of the Flag can be 0, 1,2,3,
%                             0 - detector ronchigram frames during fly-back of the probe, or frame before/after the data acquision.
%                             1,2,3 are assigned to detector frames during the probe movement. if the raw data contains a serial acquision,
%                             1,2,3 means the first, second and third image.
% RonchiRoadmap.Filename    - string cell that contains the name of the file that contains the Ronchi frame.
% RonchiRoadmap.Seq         - an integer that shows the sequence/location of the Ronchi frame inside the variable 
%                             of the corresponding containg file.
% RonchiRoadmap.Xcoord      - The Probe position the Ronchi frame corresponds to (X coordination)
% RonchiRoadmap.Ycoord      - The Probe position the Ronchi frame corresponds to (Y coordination)

% RonchiRoadmap.SurveyImage - diagnosis images obtained from the summed detector intensities

% RonchiRoadmap is also saved in the RonchiRoadmap.mat file.
    
    cd(uigetdir());

    IfExistDetfrFlag = 0;
    % first of all check if there's a RonchiRoadmap.mat file, 
    % if it exists, it means the raw-data have been organised previously
    % and can be loaded directly to save time.
    if exist('RonchiRoadmap.mat','file')   
        
        load RonchiRoadmap.mat;
        
        figure;
        NumImg = size(RonchiRoadmap.SurveyImage,3);
        for i=1:NumImg
            subplot(1,NumImg,i)
            imagesc(RonchiRoadmap.SurveyImage(:,:,i));axis image; 
        end
        
        NumImg = max(RonchiRoadmap.Flag(:));
        IfKeep = input([num2str(NumImg),'images exist, keep using the organised data? [y]/[n]:'],'s');

        if IfKeep == 'y';
            IfExistDetfrFlag = 1;
        elseif IfKeep == 'n'
%             delete('RonchiRoadmap.mat');
        else
            error('Error:Invalid input, type-in [y] or [n]');
        end
    end

    % if RonchiRoadmap not exist, or choose not to use it. 
    % Organise the ronchi-frames and generate the RonchiRoadmap file.
    if IfExistDetfrFlag == 0  

            % load the raw data files. File names start with "IntImage_" .
            files  = dir('IntImage_*.mat');
            % number of raw data files.       
            nFiles = length(files);

            NumRawFr = 1;
            for iFile = 1:nFiles 
                disp(['loading raw-data(', num2str(nFiles), '):', num2str(iFile) ]);
                img = RawFileLoaderMat(  files(iFile).name );
                files(iFile).size =  size(img,3);
                for j=1:size(img,3)
                    fr = squeeze(img(:,:,j));
                    sumInt(NumRawFr) = sum(fr(:));
                    RonchiRoadmap.SumInt = sumInt(NumRawFr);
                    NumRawFr = NumRawFr + 1;
                end
            end

            % difference in the summed intensity between adjacent detector
            % frames 
            diff_sumInt = diff(sumInt);
            RonchiRoadmap.Flag = zeros(NumRawFr,1);

    %         clear img; clear a3Tmp; clear fr;
    %         clear iFile; clear i; clear j; 


            figure; plot(sumInt); xlabel('frame number'); ylabel('sum of detector intensity') 
            % NumImg: number of images in the sequential acquision
            NumImg = input('input # of images contained in the serial acquision: ');
            % FOV: field of view: probe positions in x and y.
            FOV = input('input # of Probe Positions along x-direction (assuming Nx=Ny)? '); 
            % if hardware sync is working, there's no fly back at end of
            % each line scan.
            FlybackFlag = input('is there fly-back ronchi frames [y/n]?','s'); 
            
            endFrame = zeros(FOV,NumImg);
            
            if FlybackFlag == 'n' || FlybackFlag == 'N' 
                
                start_pix_offset = input('enter # of pixels offset in the beginning:'); 
                for iImg = 1: NumImg
                    endFrame(:,iImg) = start_pix_offset + [(FOV+(FOV*FOV*(iImg-1))) : FOV: (FOV*FOV*iImg)];
                end
    
            elseif FlybackFlag == 'y' || FlybackFlag == 'Y'
                
                for iImg = 1: NumImg
                    % now look for the frame number of the end of each line scan,
                    % When the summed detector intensities of any frame during fly-back is
                    % higher than those during the data acquision, the endFrame corresponds to where
                    % the diff_sumInt values should be positive
                    startFrame = input(['Image: ',num2str(iImg),', input any frame # during the 1st line of scan: ']);
                    temp = diff_sumInt(startFrame : length(diff_sumInt));
                    temp(temp<0)=0;
                    [~,endfr] = findpeaks(temp,'MinPeakDistance',FOV+5);
                    endFrame(:,iImg) = endfr(1:FOV) + startFrame-1;
                end
            end
            
            iFrame = 1;
            count = ones(NumImg,1);
            for iFile = 1: nFiles
                for j=1:files(iFile).size
                    for iImg = 1: NumImg
                        [~,locs] = min(abs(iFrame-squeeze(endFrame(:,iImg))));
                        if locs(1) < FOV
                            if (iFrame>endFrame(locs(1),iImg)-FOV && iFrame<=endFrame(locs(1),iImg)) ...
                                    || (iFrame>endFrame(locs(1)+1,iImg)-FOV && iFrame<=endFrame(locs(1)+1,iImg))
                                RonchiRoadmap.Flag(iFrame) = iImg;
                                RonchiRoadmap.Filename{iFrame} = files(iFile).name;
                                RonchiRoadmap.Seq(iFrame) = j;
                                RonchiRoadmap.Xcoord(iFrame) = mod(count(iImg)-1,FOV) + 1;
                                RonchiRoadmap.Ycoord(iFrame) = fix((count(iImg)-1)/FOV)+1;
                                count(iImg) = count(iImg)+1;
                            end
                        elseif locs(1) == FOV
                            if (iFrame>endFrame(locs(1),iImg)-FOV && iFrame<=endFrame(locs(1),iImg) )
                                RonchiRoadmap.Flag(iFrame) = iImg;
                                RonchiRoadmap.Filename{iFrame} = files(iFile).name;
                                RonchiRoadmap.Seq(iFrame) = j;
                                RonchiRoadmap.Xcoord(iFrame) = mod(count(iImg)-1,FOV) + 1;
                                RonchiRoadmap.Ycoord(iFrame) = fix((count(iImg)-1)/FOV)+1;
                                count(iImg) = count(iImg)+1;
                            end
                        end
                    end
                    iFrame = iFrame + 1;
                end
            end
                
          % generate synthetic diagnosis image from the summed intensities
          sumIntImg = zeros(FOV.^2,NumImg); 
          RonchiRoadmap.SurveyImage = zeros(FOV,FOV,NumImg);
          count = ones(NumImg,1);
          for iImg = 1: NumImg     
              for i=1:NumRawFr
                  if RonchiRoadmap.Flag(i) == iImg
                      sumIntImg(count(iImg),iImg) = sumInt(i);
                      count(iImg) = count(iImg)+1;
                  end
              end
              RonchiRoadmap.SurveyImage(:,:,iImg)= reshape(sumIntImg(:,iImg),FOV,FOV)';
          end
          
          save('RonchiRoadmap.mat', 'RonchiRoadmap'); 

    end

end


