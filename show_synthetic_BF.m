function [ptycho] = show_synthetic_BF(ptycho)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% To calculate and show synthethic Bright Field image. 
% Function requires structured array ptycho. Figures are displayed if
% ptycho.plot_figures is set to 1. 
% Input variables required are:
% ptycho.m: 4D array that contains the ronchigram for each probe position
% ptycho.pacbed: matrix containing the Position Averaged Convergent Beam
% Electron Diffraction (PACBED) pattern.
% ptycho.maskBF: matrix of mask for Incoherent Bright Field detector
% If ptycho.maskBF is not available, it will calculate it. This will require the
% following variables:
%   ptycho.ObjApt_angle: Probe convergence angle
%   ptycho.theta: matrix of scattering angles
% Output variables added to ptycho are:
% ptycho.BFimg: matrix of Bright Field image
% ptycho.varfunctions.show_synthetic_BF: Flag to indicate the function has been run
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ptycho.varfunctions.show_synthetic_BF %check if function has been run
    
    figure;
    imagesc(ptycho.BFimg); axis image; axis off; title('BF')
    colormap gray
    
else
    if ptycho.varfunctions.calculate_detector_masks
        maskBF = ptycho.maskBF;
    else
        ptycho = calculate_detector_masks(ptycho);
        maskBF = ptycho.maskBF;
    end
    
    % calculate the BF
    BFimg  = zeros(size(ptycho.m,3),size(ptycho.m,4));
    
    for yy=1:size(ptycho.m,3)
        for xx = 1:size(ptycho.m,4)
            %BF
            img = ptycho.m(:,:,yy,xx).*maskBF;
            BFimg(yy,xx) = sum(img(:));
        end
    end
    
    if ptycho.plot_figures
        
        figure;
        imagesc(ptycho.pacbed.*maskBF);axis image;
        title('BF detector')
        
        figure;
        imagesc(BFimg); axis image; axis off;  title('BF')
        colormap gray;
    end
    
    ptycho.BFimg = BFimg;
    ptycho.varfunctions.show_synthetic_BF = 1;
    
end

end

