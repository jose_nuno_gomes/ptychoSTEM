function [ ] = save_results_from_ptycho( ptycho, savename )
%to save the relevant variables in .mat file

% this function needs comments

% ptycho.varfunctions.load_parameters
if ptycho.varfunctions.load_data
    ptycho.m = 'removed from array to save space';
    ptycho.varfunctions.load_data = 0;
end
% ptycho.varfunctions.define_real_space
% ptycho.varfunctions.define_reciprocal_space

if ptycho.varfunctions.calculate_detector_masks
    ptycho.maskBF = 'removed from array to save space';
    ptycho.maskABF = 'removed from array to save space';
    ptycho.maskIBF = 'removed from array to save space';
    ptycho.maskDF = 'removed from array to save space';
    ptycho.maskWP = 'removed from array to save space';
    ptycho.maskPIE = 'removed from array to save space';
    ptycho.R11 = 'removed from array to save space';
    ptycho.R12 = 'removed from array to save space';
    ptycho.R13 = 'removed from array to save space';
    ptycho.R14 = 'removed from array to save space';
    ptycho.R21 = 'removed from array to save space';
    ptycho.R22 = 'removed from array to save space';
    ptycho.R23 = 'removed from array to save space';
    ptycho.R24 = 'removed from array to save space';
    ptycho.R31 = 'removed from array to save space';
    ptycho.R32 = 'removed from array to save space';
    ptycho.R33 = 'removed from array to save space';
    ptycho.R34 = 'removed from array to save space';
    ptycho.varfunctions.calculate_detector_masks = 0;
end
% ptycho.varfunctions.show_synthetic_IBF
% ptycho.varfunctions.show_synthetic_BF
% ptycho.varfunctions.show_synthetic_ABF
% ptycho.varfunctions.show_synthetic_DF
% ptycho.varfunctions.show_synthetic_DPC
% ptycho.varfunctions.calculate_center_of_mass
if ptycho.varfunctions.truncate_ronchigrams_within_collection_angle
    ptycho.m_wp = 'removed from array to save space';
    ptycho.varfunctions.truncate_ronchigrams_within_collection_angle = 0;
end
% ptycho.varfunctions.define_reciprocal_space_for_truncated_detector_plane
if ptycho.varfunctions.FT_from_M_to_G
    ptycho.G_wp = 'removed from array to save space';  
    ptycho.varfunctions.FT_from_M_to_G = 0; 
end
% ptycho.varfunctions.single_side_band_reconstruction
% ptycho.varfunctions.define_reciprocal_space_resampled_ronchigram
% ptycho.varfunctions.define_probe_function
if ptycho.varfunctions.FT_from_G_to_H
    ptycho.H = 'removed from array to save space';
    ptycho.varfunctions.FT_from_G_to_H = 0;
end

% ptycho.varfunctions.wigner_distribution_deconvolution_reconstruction

save(savename,'ptycho');

display('variable ptycho has been saved to');
display({savename});

end

